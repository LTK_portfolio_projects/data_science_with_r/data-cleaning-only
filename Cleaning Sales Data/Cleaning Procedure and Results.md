# Data Cleaning Documentation for Sales Data

## Overview

This document outlines the data cleaning steps applied to the sales data from XYZ company. The goal is to prepare the dataset for further analysis by addressing inconsistencies, missing values, and formatting issues.

## Dataset Description

The dataset contains sales records with the following key columns: - `Store_ID` - `Product_ID` - `Date` - `Sales_Amount` - `Customer_Age_Group` - `Payment_Type` - `Quantity`

## Cleaning Steps

### 1. Handling Missing Values

-   **Product_ID**: Missing `Product_ID` values were identified and handled. [Reference to specific code part]
-   **Store_ID**: Missing values in `Store_ID` were addressed. [Reference to specific code part]

### 2. Standardizing Date Formats

-   The `Date` column had multiple formats which were standardized to "YYYY-MM-DD".

### 3. Correcting `Sales_Amount`

-   Textual representations in `Sales_Amount` like "hundred", "fifty" were converted to numeric values.
-   In this scenario, negative values in `Sales_Amount`, representing returns or refunds, were handled.
    -   A new column named `Trasction_Type` which consisted of `Sales` or `Refunds` are created.

### 4. Categorizing `Customer_Age_Group`

-   Age groups were standardized, in which all age ranging (i.e. "18-25", "26-35", "36-45", ...etc) were categorized into 3 groups (i.e. `young`, `middle`, `old`).

### 5. Cleaning the `Quantity` Column

-   Textual representations in the `Quantity` column, specifically the term "multiple", were converted to `NA` to indicate the ambiguity in the quantity.
-   The total and percentage of missing (`NA`) records in the `Quantity` column were calculated to assess data quality.

### 6. Splitting Data into Good and Bad Sets

-   The dataset was split into two subsets: `cleaned_data` for records without any missing values and suitable for analysis, and `bad_data` for records that require further review or imputation.
-   The total count of `NA` values was reassessed after splitting the dataset.

### 7. Sorting Cleaned Data

-   The cleaned dataset (`cleaned_data`) was sorted based on `Store_ID`, `Transaction_Type`, `Customer_Age_Group`, `Date`, `Quantity`, and `Sales_Amount` to organize the data for analysis.

### 8. Saving Datasets

-   The cleaned data (`cleaned_data`) was saved to a file named `cleaned_sales_data.csv`.
-   The data requiring review or update (`bad_data`) was saved to a file named `need_review_sales_data.csv`.

## Conclusion

The cleaned dataset is now ready for further analysis, with improved consistency and reliability for business insights. """

# Original Messy Data (Before)

| Store_ID | Product_ID | Date       | Sales_Amount       | Customer_Age_Group | Payment_Type | Quantity |
|----------|------------|------------|--------------------|--------------------|--------------|----------|
| S_001    |            | 2/07/2023  | 224.43145054410624 | 36-45              | Cash         | 3        |
| S_001    |            | 04/29/2023 | 289.2671632203255  | 18-25              | cash         | 7        |
| S_001    | P_00003    | 9/06/2023  | -20.7097321        | middle             | Credit Card  | 5        |
| S_001    | P_00004    | 6/03/2023  | 114.7740229        | 36-45              | credit       | 4        |
| S_001    | P_00005    | 4/02/2023  | 195.5851274112644  | \>65               | credit       | 2        |
| S_001    | P_00006    | 07/13/2023 | 242.8528581667917  | 46-55              | cash         | 1        |
| S_001    | P_00007    | 6/08/2023  | 364.54283710176094 | middle             | Credit Card  | 2        |
| S_001    |            | 7/07/2023  | 347.12046867566255 | young              | Credit Card  | 6        |
| S_001    | P_00009    | 19/11/2023 | 262.0242534294088  | 46-55              | credit       | 3        |
| S_001    | P_00010    | 12/10/2023 | 279.0369462682129  | 18-25              | cash         | 2        |
| S_001    | P_00011    | 14/08/2023 | -115.6093551       | \>65               | Debit        | 5        |
| S_001    | P_00012    | 6/01/2023  | 446.71148765816446 | middle             | Cash         | 6        |
| S_001    | P_00013    | 13/02/2023 | 43.73417757607772  | 56-65              | Cash         | 10       |
| S_001    | P_00014    | 21/02/2023 | -130.9923759       | young              | Credit Card  | 4        |
| S_001    | P_00015    | 25/09/2023 | 147.9984331669776  | 18-25              | credit       | 8        |
| S_001    | P_00016    | 3/11/2023  | 178.1469382974082  | 18-25              | Cash         | 3        |
| S_001    | P_00017    | 17/01/2023 | 295.66924856396696 | middle             | cash         | 4        |
| S_001    | P_00018    | 11/01/2023 | 484.1387760565548  | middle             | credit       | 6        |
| S_001    | P_00019    | 04/24/2023 | 458.6941202748346  | \>65               | debit        | 2        |
| S_001    | P_00020    | 24/03/2023 | 160.85154501126624 | \>65               | debit        | 10       |
| S_001    | P_00021    | 30/10/2023 | 163.73704617193766 | \>65               | Debit        | 5        |
| S_001    | P_00022    | 13/02/2023 | 446.8652416556285  | 18-25              | Cash         | 5        |
| S_001    |            | 26/10/2023 | -322.7582905       | 46-55              | cash         | 3        |
| S_001    |            | 12/09/2023 | 383.0031278555348  | 46-55              | cash         | 7        |
| S_002    | P_00025    | 6/03/2023  | 480.1884715390365  | young              | Credit Card  | 5        |
| S_002    | P_00026    | 25/09/2023 | 377.5350523        | 18-25              | Credit Card  | 1        |
| S_002    | P_00027    | 06/13/2023 | 308.36215847615256 | young              | credit       | 7        |
| S_002    | P_00028    | 16/10/2023 | 228.6913923763209  | \>65               | Credit Card  | 1        |
| S_002    | P_00029    | 25/10/2023 | 158.20426063306516 | 18-25              | Debit        | 2        |
| S_002    | P_00030    | 19/12/2023 | 129.98865094150273 | old                | debit        | 8        |
| S_002    | P_00031    | 10/27/2023 | 100.40341610825419 | middle             | credit       | 2        |
| S_002    | P_00032    | 28/06/2023 | 481.6704832982175  | young              | credit       | 5        |
| S_002    |            | 5/09/2023  | 77.64956100688369  | old                | cash         | 3        |
| S_002    | P_00034    | 6/04/2023  | 454.9768645667928  | 36-45              | credit       | 5        |
| S_002    | P_00035    | 12/22/2023 | thousand           | 36-45              | cash         | 3        |
| S_002    | P_00036    | 16/11/2023 | 236.16988320474553 | young              | Cash         | 7        |
| S_002    | P_00037    | 14/02/2023 | \-                 | \>65               | Credit Card  | 7        |
| S_002    | P_00038    | 08/26/2023 | -338.8504712       | 36-45              | debit        | 2        |
| S_002    | P_00039    | 28/07/2023 | hundred            | 18-25              | Credit Card  | 7        |
| S_002    | P_00040    | 11/03/2023 | 230.2787724087421  | 46-55              | Debit        | 5        |
| S_002    | P_00041    | 24/04/2023 | 205.02174590997942 | young              | Debit        | 10       |
| S_002    | P_00042    | 03/20/2023 | -134.8833143       | 26-35              | Debit        | 1        |
| S_002    | P_00043    | 28/03/2023 | -353.0970912       | middle             | credit       | 5        |
| S_002    | P_00044    | 01/21/2023 | -495.3066309       | middle             | Cash         | 2        |
| S_002    | P_00045    | 3/04/2023  | -315.6434521       | middle             | Credit Card  | 7        |
| S_002    | P_00046    | 9/07/2023  | 267.5017014320623  | 56-65              | Debit        | 7        |
| S_002    | P_00047    | 7/09/2023  | 326.8621639494713  | 18-25              | cash         | 7        |
| S_002    |            | 1/04/2023  | fifty              | 56-65              | Credit Card  | 1        |
| S_002    | P_00049    | 10/14/2023 | 164.30625811414114 | \>65               | credit       | 7        |
| S_003    | P_00050    | 29/08/2023 | 457.81989988325125 | 36-45              | cash         | 7        |
| S_003    | P_00051    | 31/08/2023 | 128.06152254188208 | 36-45              | credit       | 8        |
| S_003    | P_00052    | 12/08/2023 | -490.5354554       | 18-25              | Cash         | 9        |
| S_003    | P_00053    | 22/02/2023 | 461.4643170946108  | 56-65              | Credit Card  | 8        |
| S_003    | P_00054    | 14/10/2023 | 452.0455781059004  | young              | Debit        | 2        |
| S_003    | P_00055    | 11/20/2023 | 295.9348109308908  | middle             | Cash         | 5        |
| S_003    |            | 3/02/2023  | 379.8509292781875  | 36-45              | Cash         | 5        |
| S_003    | P_00057    | 02/27/2023 | -281.2837174       | middle             | credit       | 10       |
| S_003    | P_00058    | 24/04/2023 | -381.5674236       | 18-25              | cash         | 1        |
| S_003    | P_00059    | 09/25/2023 | 407.98221079121015 | \>65               | cash         | 2        |
| S_003    | P_00060    | 10/14/2023 | 471.8055952574871  | middle             | cash         | multiple |
| S_003    | P_00061    | 19/09/2023 | 213.0522312697673  | 36-45              | debit        | 5        |
| S_003    | P_00062    | 06/14/2023 | 348.81669876556947 | middle             | Cash         | 3        |
| S_003    | P_00063    | 17/07/2023 | -215.206179        | 18-25              | Debit        | 8        |
| S_003    | P_00064    | 11/29/2023 | 273.44178115390724 | 18-25              | debit        | 7        |
| S_003    | P_00065    | 22/02/2023 | 111.33349732407255 | 56-65              | debit        | 9        |
| S_003    | P_00066    | 07/31/2023 | 409.6470001        | 26-35              | credit       | 10       |
| S_003    | P_00067    | 2/08/2023  | 79.74011138964796  | 18-25              | Credit Card  | 10       |
| S_003    | P_00068    | 18/10/2023 | 204.73174840010978 | young              | credit       | 5        |
| S_003    | P_00069    | 7/03/2023  | 293.8290104881941  | 26-35              | cash         | 4        |
| S_003    | P_00070    | 20/01/2023 | 294.44844166458745 | middle             | Cash         | 10       |
| S_003    | P_00071    | 1/01/2024  | 289.0440866874169  | old                | debit        | 1        |
| S_003    | P_00072    | 13/10/2023 | hundred            | 56-65              | debit        | multiple |
| S_003    | P_00073    | 23/11/2023 | 226.62141698822614 | 18-25              | debit        | 6        |
| S_003    | P_00074    | 12/21/2023 | 309.11027172816966 | 26-35              | Cash         | 8        |
| S_004    | P_00075    | 31/07/2023 | -331.3714779       | old                | cash         | 10       |
| S_004    | P_00076    | 12/01/2023 | 469.12571315961947 | 18-25              | debit        | 4        |
| S_004    |            | 22/10/2023 | 149.26661088433724 | old                | Credit Card  | 6        |
| S_004    | P_00078    | 2/01/2023  | -128.8222896       | middle             | Debit        | 2        |
| S_004    | P_00079    | 16/10/2023 | 82.66453626585206  | 18-25              | credit       | 6        |
| S_004    | P_00080    | 16/09/2023 | -372.3573644       | 46-55              | cash         | 1        |
| S_004    | P_00081    | 4/08/2023  | 182.6867562086847  | middle             | Cash         | 8        |
| S_004    | P_00082    | 26/08/2023 | 152.53549465058538 | 26-35              | credit       | 9        |
| S_004    | P_00083    | 28/05/2023 | 365.30739363854264 | \>65               | credit       | 4        |
| S_004    | P_00084    | 26/02/2023 | 390.1593509011946  | 26-35              | credit       | 9        |
| S_004    | P_00085    | 2/07/2023  | 189.6536062571622  | 18-25              | Credit Card  | 3        |
| S_004    | P_00086    | 20/02/2023 | 353.7956881898388  | old                | cash         | 4        |
| S_004    | P_00087    | 10/03/2023 | 415.7567822        | \>65               | Cash         | 7        |
| S_004    | P_00088    | 16/07/2023 | 23.269337656991667 | 36-45              | Cash         | 8        |
| S_004    | P_00089    | 16/08/2023 | -324.0907554       | old                | cash         | 1        |
| S_004    | P_00090    | 4/04/2023  | 299.89580398291787 | old                | Credit Card  | 5        |
| S_004    | P_00091    | 10/02/2023 | -79.9508458        | 56-65              | Debit        | 5        |
| S_004    | P_00092    | 3/11/2023  | thousand           | young              | debit        | 3        |
| S_004    | P_00093    | 28/09/2023 | 319.6995277476302  | middle             | cash         | 3        |
| S_004    | P_00094    | 16/09/2023 | 32.72468386023773  | 26-35              | Cash         | multiple |
| S_004    | P_00095    | 25/09/2023 | 329.82324770462145 | 36-45              | Cash         | 8        |
| S_004    | P_00096    | 17/11/2023 | -472.9449594       | \>65               | credit       | 10       |
| S_004    | P_00097    | 28/01/2023 | 161.3641667486315  | old                | Credit Card  | 1        |
| S_004    | P_00098    | 15/09/2023 | 307.0927368593976  | 46-55              | cash         | 7        |
| S_004    | P_00099    | 4/03/2023  | 333.64926419991104 | 46-55              | Cash         | 10       |
| S_100    | P_00100    | 10/31/2023 | 94.65724038383317  | \>65               | Cash         | 4        |

# Cleaned Data (After)

| Store_ID | Product_ID | Date       | Sales_Amount | Customer_Age_Group | Payment_Type | Quantity | Transaction_Type |
|----------|------------|------------|--------------|--------------------|--------------|----------|------------------|
| S_001    | P_00012    | 2023-01-06 | 446.71       | middle             | Cash         | 6        | Sale             |
| S_001    | P_00018    | 2023-01-11 | 484.14       | middle             | credit       | 6        | Sale             |
| S_001    | P_00017    | 2023-01-17 | 295.67       | middle             | cash         | 4        | Sale             |
| S_001    | P_00005    | 2023-02-04 | 195.59       | old                | credit       | 2        | Sale             |
| S_001    | P_00013    | 2023-02-13 | 43.73        | old                | Cash         | 10       | Sale             |
| S_001    | P_00022    | 2023-02-13 | 446.87       | young              | Cash         | 5        | Sale             |
| S_001    | P_00014    | 2023-02-21 | 130.99       | young              | Credit Card  | 4        | Refund           |
| S_001    | P_00004    | 2023-03-06 | 114.77       | middle             | credit       | 4        | Sale             |
| S_001    | P_00020    | 2023-03-24 | 160.85       | old                | debit        | 10       | Sale             |
| S_001    | P_00019    | 2023-04-24 | 458.69       | old                | debit        | 2        | Sale             |
| S_001    | P_00003    | 2023-06-09 | 20.71        | middle             | Credit Card  | 5        | Refund           |
| S_001    | P_00006    | 2023-07-13 | 242.85       | middle             | cash         | 1        | Sale             |
| S_001    | P_00007    | 2023-08-06 | 364.54       | middle             | Credit Card  | 2        | Sale             |
| S_001    | P_00011    | 2023-08-14 | 115.61       | old                | Debit        | 5        | Refund           |
| S_001    | P_00015    | 2023-09-25 | 148          | young              | credit       | 8        | Sale             |
| S_001    | P_00010    | 2023-10-12 | 279.04       | young              | cash         | 2        | Sale             |
| S_001    | P_00021    | 2023-10-30 | 163.74       | old                | Debit        | 5        | Sale             |
| S_001    | P_00016    | 2023-11-03 | 178.15       | young              | Cash         | 3        | Sale             |
| S_001    | P_00009    | 2023-11-19 | 262.02       | middle             | credit       | 3        | Sale             |
| S_002    | P_00044    | 2023-01-21 | 495.31       | middle             | Cash         | 2        | Refund           |
| S_002    | P_00025    | 2023-03-06 | 480.19       | young              | Credit Card  | 5        | Sale             |
| S_002    | P_00040    | 2023-03-11 | 230.28       | middle             | Debit        | 5        | Sale             |
| S_002    | P_00042    | 2023-03-20 | 134.88       | young              | Debit        | 1        | Refund           |
| S_002    | P_00043    | 2023-03-28 | 353.1        | middle             | credit       | 5        | Refund           |
| S_002    | P_00045    | 2023-04-03 | 315.64       | middle             | Credit Card  | 7        | Refund           |
| S_002    | P_00034    | 2023-04-06 | 454.98       | middle             | credit       | 5        | Sale             |
| S_002    | P_00041    | 2023-04-24 | 205.02       | young              | Debit        | 10       | Sale             |
| S_002    | P_00027    | 2023-06-13 | 308.36       | young              | credit       | 7        | Sale             |
| S_002    | P_00032    | 2023-06-28 | 481.67       | young              | credit       | 5        | Sale             |
| S_002    | P_00046    | 2023-07-09 | 267.5        | old                | Debit        | 7        | Sale             |
| S_002    | P_00039    | 2023-07-28 | 100          | young              | Credit Card  | 7        | Sale             |
| S_002    | P_00038    | 2023-08-26 | 338.85       | middle             | debit        | 2        | Refund           |
| S_002    | P_00047    | 2023-09-07 | 326.86       | young              | cash         | 7        | Sale             |
| S_002    | P_00026    | 2023-09-25 | 377.54       | young              | Credit Card  | 1        | Sale             |
| S_002    | P_00049    | 2023-10-14 | 164.31       | old                | credit       | 7        | Sale             |
| S_002    | P_00028    | 2023-10-16 | 228.69       | old                | Credit Card  | 1        | Sale             |
| S_002    | P_00029    | 2023-10-25 | 158.2        | young              | Debit        | 2        | Sale             |
| S_002    | P_00031    | 2023-10-27 | 100.4        | middle             | credit       | 2        | Sale             |
| S_002    | P_00036    | 2023-11-16 | 236.17       | young              | Cash         | 7        | Sale             |
| S_002    | P_00030    | 2023-12-19 | 129.99       | old                | debit        | 8        | Sale             |
| S_002    | P_00035    | 2023-12-22 | 1000         | middle             | cash         | 3        | Sale             |
| S_003    | P_00070    | 2023-01-20 | 294.45       | middle             | Cash         | 10       | Sale             |
| S_003    | P_00053    | 2023-02-22 | 461.46       | old                | Credit Card  | 8        | Sale             |
| S_003    | P_00065    | 2023-02-22 | 111.33       | old                | debit        | 9        | Sale             |
| S_003    | P_00057    | 2023-02-27 | 281.28       | middle             | credit       | 10       | Refund           |
| S_003    | P_00069    | 2023-03-07 | 293.83       | young              | cash         | 4        | Sale             |
| S_003    | P_00058    | 2023-04-24 | 381.57       | young              | cash         | 1        | Refund           |
| S_003    | P_00062    | 2023-06-14 | 348.82       | middle             | Cash         | 3        | Sale             |
| S_003    | P_00063    | 2023-07-17 | 215.21       | young              | Debit        | 8        | Refund           |
| S_003    | P_00066    | 2023-07-31 | 409.65       | young              | credit       | 10       | Sale             |
| S_003    | P_00067    | 2023-08-02 | 79.74        | young              | Credit Card  | 10       | Sale             |
| S_003    | P_00052    | 2023-08-12 | 490.54       | young              | Cash         | 9        | Refund           |
| S_003    | P_00050    | 2023-08-29 | 457.82       | middle             | cash         | 7        | Sale             |
| S_003    | P_00051    | 2023-08-31 | 128.06       | middle             | credit       | 8        | Sale             |
| S_003    | P_00061    | 2023-09-19 | 213.05       | middle             | debit        | 5        | Sale             |
| S_003    | P_00059    | 2023-09-25 | 407.98       | old                | cash         | 2        | Sale             |
| S_003    | P_00054    | 2023-10-14 | 452.05       | young              | Debit        | 2        | Sale             |
| S_003    | P_00068    | 2023-10-18 | 204.73       | young              | credit       | 5        | Sale             |
| S_003    | P_00055    | 2023-11-20 | 295.93       | middle             | Cash         | 5        | Sale             |
| S_003    | P_00073    | 2023-11-23 | 226.62       | young              | debit        | 6        | Sale             |
| S_003    | P_00064    | 2023-11-29 | 273.44       | young              | debit        | 7        | Sale             |
| S_003    | P_00074    | 2023-12-21 | 309.11       | young              | Cash         | 8        | Sale             |
| S_003    | P_00071    | 2024-01-01 | 289.04       | old                | debit        | 1        | Sale             |
| S_004    | P_00078    | 2023-01-02 | 128.82       | middle             | Debit        | 2        | Refund           |
| S_004    | P_00076    | 2023-01-12 | 469.13       | young              | debit        | 4        | Sale             |
| S_004    | P_00097    | 2023-01-28 | 161.36       | old                | Credit Card  | 1        | Sale             |
| S_004    | P_00091    | 2023-02-10 | 79.95        | old                | Debit        | 5        | Refund           |
| S_004    | P_00086    | 2023-02-20 | 353.8        | old                | cash         | 4        | Sale             |
| S_004    | P_00084    | 2023-02-26 | 390.16       | young              | credit       | 9        | Sale             |
| S_004    | P_00099    | 2023-03-04 | 333.65       | middle             | Cash         | 10       | Sale             |
| S_004    | P_00087    | 2023-03-10 | 415.76       | old                | Cash         | 7        | Sale             |
| S_004    | P_00090    | 2023-04-04 | 299.9        | old                | Credit Card  | 5        | Sale             |
| S_004    | P_00083    | 2023-05-28 | 365.31       | old                | credit       | 4        | Sale             |
| S_004    | P_00085    | 2023-07-02 | 189.65       | young              | Credit Card  | 3        | Sale             |
| S_004    | P_00088    | 2023-07-16 | 23.27        | middle             | Cash         | 8        | Sale             |
| S_004    | P_00075    | 2023-07-31 | 331.37       | old                | cash         | 10       | Refund           |
| S_004    | P_00081    | 2023-08-04 | 182.69       | middle             | Cash         | 8        | Sale             |
| S_004    | P_00089    | 2023-08-16 | 324.09       | old                | cash         | 1        | Refund           |
| S_004    | P_00082    | 2023-08-26 | 152.54       | young              | credit       | 9        | Sale             |
| S_004    | P_00098    | 2023-09-15 | 307.09       | middle             | cash         | 7        | Sale             |
| S_004    | P_00080    | 2023-09-16 | 372.36       | middle             | cash         | 1        | Refund           |
| S_004    | P_00095    | 2023-09-25 | 329.82       | middle             | Cash         | 8        | Sale             |
| S_004    | P_00093    | 2023-09-28 | 319.7        | middle             | cash         | 3        | Sale             |
| S_004    | P_00079    | 2023-10-16 | 82.66        | young              | credit       | 6        | Sale             |
| S_004    | P_00092    | 2023-11-03 | 1000         | young              | debit        | 3        | Sale             |
| S_004    | P_00096    | 2023-11-17 | 472.94       | old                | credit       | 10       | Refund           |
| S_100    | P_00100    | 2023-10-31 | 94.66        | old                | Cash         | 4        | Sale             |

# Needed Review Sales Data (After)

| Store_ID | Product_ID | Date       | Sales_Amount | Customer_Age_Group | Payment_Type | Quantity | Transaction_Type |
|----------|------------|------------|--------------|--------------------|--------------|----------|------------------|
| S_001    | NA         | 2023-07-02 | 224.43       | middle             | Cash         | 3        | Sale             |
| S_001    | NA         | 2023-04-29 | 289.27       | young              | cash         | 7        | Sale             |
| S_001    | NA         | 2023-07-07 | 347.12       | young              | Credit Card  | 6        | Sale             |
| S_001    | NA         | 2023-10-26 | 322.76       | middle             | cash         | 3        | Refund           |
| S_001    | NA         | 2023-09-12 | 383          | middle             | cash         | 7        | Sale             |
| S_002    | NA         | 2023-09-05 | 77.65        | old                | cash         | 3        | Sale             |
| S_002    | P_00037    | 2023-02-14 | NA           | old                | Credit Card  | 7        | NA               |
| S_002    | NA         | 2023-04-01 | 50           | old                | Credit Card  | 1        | Sale             |
| S_003    | NA         | 2023-02-03 | 379.85       | middle             | Cash         | 5        | Sale             |
| S_003    | P_00060    | 2023-10-14 | 471.81       | middle             | cash         | NA       | Sale             |
| S_003    | P_00072    | 2023-10-13 | 100          | old                | debit        | NA       | Sale             |
| S_004    | NA         | 2023-10-22 | 149.27       | old                | Credit Card  | 6        | Sale             |
| S_004    | P_00094    | 2023-09-16 | 32.72        | young              | Cash         | NA       | Sale             |
